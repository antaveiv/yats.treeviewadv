using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;

namespace Aga.Controls.Tree
{
	/// <summary>
	/// Provides a simple ready to use implementation of <see cref="ITreeModel"/>. Warning: this class is not optimized 
	/// to work with big amount of data. In this case create you own implementation of <c>ITreeModel</c>, and pay attention
	/// on GetChildren and IsLeaf methods.
	/// </summary>
    public class TreeModel<T> : ITreeModel where T : Node<T>, new()
	{
        private T _root;
        public T Root
		{
			get { return _root; }
		}

        public Collection<T> Nodes
		{
			get { return _root.Nodes; }
		}

		public TreeModel()
		{
            _root = new T();
			_root.Model = this;
		}

        public TreePath GetPath(Node<T> node)
		{
			if (node == _root)
				return TreePath.Empty;
			else
			{
				Stack<object> stack = new Stack<object>();
				while (node != _root)
				{
					stack.Push(node);
					node = node.Parent;
				}
				return new TreePath(stack.ToArray());
			}
		}

        public Node<T> FindNode(TreePath path)
		{
			if (path.IsEmpty())
				return _root;
			else
				return FindNode(_root, path, 0);
		}

        private Node<T> FindNode(Node<T> root, TreePath path, int level)
		{
            foreach (Node<T> node in root.Nodes)
				if (node == path.FullPath[level])
				{
					if (level == path.FullPath.Length - 1)
						return node;
					else
						return FindNode(node, path, level + 1);
				}
			return null;
		}

		#region ITreeModel Members

		public virtual System.Collections.IEnumerable GetChildren(TreePath treePath)
		{
            Node<T> node = FindNode(treePath);
			if (node != null)
                foreach (Node<T> n in node.Nodes)
					yield return n;
			else
				yield break;
		}

		public virtual bool IsLeaf(TreePath treePath)
		{
            Node<T> node = FindNode(treePath);
			if (node != null)
				return node.IsLeaf;
			else
				throw new ArgumentException("treePath");
		}

		public event EventHandler<TreeModelEventArgs> NodesChanged;
		public void OnNodesChanged(TreeModelEventArgs args)
		{
			if (NodesChanged != null)
				NodesChanged(this, args);
		}

        public void OnNodesChanged(T node)
        {
            if(NodesChanged != null)
            {
                var parent = node.Parent;
                int index = parent.Nodes.IndexOf( node );
                if(index < 0)
                {
                    return;
                }
                TreeModelEventArgs args = new TreeModelEventArgs( GetPath( parent ), new int[] { index }, new object[] { node } );
                NodesChanged( this, args );
            }
        }
		public event EventHandler<TreePathEventArgs> StructureChanged;
		public void OnStructureChanged(TreePathEventArgs args)
		{
			if (StructureChanged != null)
				StructureChanged(this, args);
		}

		public event EventHandler<TreeModelEventArgs> NodesInserted;
        public void OnNodeInserted(Node<T> parent, int index, Node<T> node)
		{
			if (NodesInserted != null)
			{
				TreeModelEventArgs args = new TreeModelEventArgs(GetPath(parent), new int[] { index }, new object[] { node });
				NodesInserted(this, args);
			}

		}

		public event EventHandler<TreeModelEventArgs> NodesRemoved;
        public void OnNodeRemoved(Node<T> parent, int index, Node<T> node)
		{
			if (NodesRemoved != null)
			{
				TreeModelEventArgs args = new TreeModelEventArgs(GetPath(parent), new int[] { index }, new object[] { node });
				NodesRemoved(this, args);
			}
		}

        public void OnNodeRemoved(T node)
		{
			if (NodesRemoved != null)
			{
                var parent = node.Parent;
                int index = parent.Nodes.IndexOf(node);
                if (index < 0)
                {
                    return;
                }
                TreeModelEventArgs args = new TreeModelEventArgs(GetPath(parent), new int[] { index }, new object[] { node });

				NodesRemoved(this, args);
			}
		}

		#endregion
	}
}
